package ProgramaJoel;

import java.util.HashMap;
import java.util.Scanner;
	/**
		 * @author Chen Chen
		 * @version 1.0.
		 * @since 17/01/2024.
		 * 
		 * Aquest joc és una clara còpia del joc Lethal Company. 
		 * Tens una quota a superar i uns dies per recolectar objectes
		 * i després per vendre-ho
		 * Sí superes la quota sobreviuras i si no perdràs
		 */

public class Problema {
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		jugar();
	}

	public static void jugar() {
		boolean sortir = true;
		

			//Aquesta variable serveix per utilitzar-se com un acumulador
			int DinersTotal = 0;
			
			
			int quota = 0;
			
			quota = quota(quota);
			
			boolean confirmar = false;
			
			confirmar = quotaacceptable(quota);
			
			while( confirmar == false) {
				System.out.println("NO");
				quota = sc.nextInt();
				confirmar = quotaacceptable(quota);
			} 
					
			int dias = 0;
			
			dias = dias(dias);
		
			DinersTotal = partida(quota, dias, DinersTotal);
			
			boolean partida = conf(DinersTotal, quota);
			
			print(partida);
		
	}

	public static String print(boolean partida) {
		if(partida == true) {
			System.out.println("OK");
			return "OK";
		}else {
			System.out.println("MAL");
			return "MAL";
		}
	}

	public static boolean conf(int DinersTotal, int quota) {
		if (DinersTotal >= quota) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean quotaacceptable(int quota) {
		if ( quota <= 0 ) {
			return false;
		}else {
			return true;
		}

	}
	public static int objetos(int objetos) {
		//La quantitat d'objectes que anas agafant durant cada dia
		objetos = sc.nextInt();
		return objetos;
	}

	public static int dias(int dias) {
		//Els dies que tens per agafar
			dias = sc.nextInt();
			return dias;
	}

	/**
	 * Aixó serveix per a que la quota no sigui menor o igual a 0
	 * @param r 
	 * 
	 * @param quota
	 * @return la quantitat que has possat
	 */
	public static int quota(int quota) {
		//Insereixes una quantitat 
		quota = sc.nextInt();
		return quota;
	}
	
	/**
	 * Aquesta funcio funciona amb un bucle, insereixes un nom + un guio + la quantitat que dona aquell objecte,
	 * per exemple, "nom"-"quantitat", fem un split on afegim el nom al diccionari i la quantitat en el acumulador
	 * després farà una comparació de la quota amb el diners total
	 * 
	 * @param Quota Quantitat a superar o igualar
	 * @param Dies Els dies que tens per recolectar
	 * @param Objectes Quantitat d'objectes per recolectar al dia
	 * @param DinersTotal La quantitat total de diners que has anat aconseguit
	 * @return Et dira si has sobreviscut o has perdut
	 */
	public static int partida(int quota, int dias, int DinersTotal ) {
		for (int u = 0; u < dias; u++) {
			int objetos = 0;
			objetos = objetos(objetos);
			for( int i = 0 ; i < objetos ; i++) {
				int quantitat = sc.nextInt();
				DinersTotal = DinersTotal + quantitat;
			}
		}return DinersTotal;
	
	}

	
}
