package ProgramaJoel;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

class caso1 {
	static Scanner sc = new Scanner(System.in);

	int quota;

	private boolean comprobarquota(int quota) {
		if (Problema.quotaacceptable(quota) == true) {
			return true;
		} else {
			return false;
		}
	}

	HashMap<String, Integer> Objetos = new HashMap<>();

	@Test
	void cas1() {
		int quota = Problema.quota(100);

		assertTrue(Problema.quotaacceptable(quota));

		int resul = Problema.dias(1);
		int resul2 = Problema.objetos(5); 
		int acum = 0;
		for ( int u = 0; u < resul ; u++) {
			for ( int i = 0; i < resul2 ; i++) {
				acum = acum + 20;
			}
		}
		boolean partida = Problema.conf(acum, quota);

		assertEquals(true, partida);
		
		String resultat = Problema.print(partida);
		
		assertEquals("OK", resultat);
	}

	@Test
	void cas2() {
		int quota = Problema.quota(0);

		assertFalse(Problema.quotaacceptable(quota));
		
		quota = Problema.quota(100);
		
		assertTrue(Problema.quotaacceptable(quota));
		
		int resul = Problema.dias(1);
		int resul2 = Problema.objetos(5); 
		int acum = 0;
		for ( int u = 0; u < resul ; u++) {
			for ( int i = 0; i < resul2 ; i++) {
				acum = acum + 20;
			}
		}
		boolean partida = Problema.conf(acum, quota);

		assertEquals(true, partida);
		
		String resultat = Problema.print(partida);
		
		assertEquals("OK", resultat);
	}

	@Test
	void cas3() {
		int quota = Problema.quota(10000);

		assertTrue(Problema.quotaacceptable(quota));

		int resul = Problema.dias(1);
		int resul2 = Problema.objetos(5); 
		int acum = 0;
		for ( int u = 0; u < resul ; u++) {
			for ( int i = 0; i < resul2 ; i++) {
				acum = acum + 20;
			}
		}
		boolean partida = Problema.conf(acum, quota);

		assertEquals(false, partida);
		
		String resultat = Problema.print(partida);
		
		assertEquals("MAL", resultat);
	}

	@Test
	void cas4() {
		int quota = Problema.quota(200);

		assertTrue(Problema.quotaacceptable(quota));

		int resul = Problema.dias(1);
		int resul2 = Problema.objetos(5); 
		int acum = 0;
		for ( int u = 0; u < resul ; u++) {
			for ( int i = 0; i < resul2 ; i++) {
				acum = acum - 20;
			}
		}
		boolean partida = Problema.conf(acum, quota);

		assertEquals(false, partida);
		
		String resultat = Problema.print(partida);
		
		assertEquals("MAL", resultat);
	}

	@Test
	void cas5() {
		int quota = Problema.quota(-200);

		assertFalse(Problema.quotaacceptable(quota));
		
		quota = Problema.quota(200);

		assertTrue(Problema.quotaacceptable(quota));

		int resul = Problema.dias(2);
		int resul2 = Problema.objetos(5); 
		int acum = 0;
		for ( int u = 0; u < resul ; u++) {
			for ( int i = 0; i < resul2 ; i++) {
				acum = acum + 20;
			}
		}
		boolean partida = Problema.conf(acum, quota);

		assertEquals(true, partida);
		
		String resultat = Problema.print(partida);
		
		assertEquals("OK", resultat);
	}

	@Test
	void cas6() {
		int quota = Problema.quota(-100);

		asserFalse(Problema.quotaacceptable(quota));

		quota = Problema.quota(700);
		
		int resul = Problema.dias(2);
		int resul2 = Problema.objetos(5); 
		int acum = 0;
		for ( int u = 0; u < resul ; u++) {
			for ( int i = 0; i < resul2 ; i++) {
				acum = acum + 30;
			}
		}
		boolean partida = Problema.conf(acum, quota);

		assertEquals(false, partida);
		
		String resultat = Problema.print(partida);
		
		assertEquals("MAL", resultat);
	}

}
